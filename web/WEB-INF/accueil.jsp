<%-- 
    Document   : accueil
    Created on : 7 juin 2021, 15:11:53
    Author     : vincent
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="fr.ldnr.app.modele.*"
         import="java.util.*"
         import="java.util.stream.Collectors"
         %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%!
    Competiteurs competiteurs;
    Pays pays;
    List<Pays> listePays;
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="/SuperApp/css/style.css">
    </head>
    <body>
        <%@ include file="menu.jsp" %>


        <div>
            <h1>Pays engagés</h1>
           <!-- <div>
                <table>

                    <tr><td colspan="2"><h1>Méthode 1 : Scriptlet, utilisation du List</h1></td></tr>
                    <%
                        competiteurs = (Competiteurs) application.getAttribute("competiteurs");
                        final int NBMAX = competiteurs.getNbCompetiteurs();
                        listePays = competiteurs.getPaysL();
                        //Collections.sort(listePays, (o1, o2) -> o1.getNom().compareTo(o2.getNom()));
                        Collections.sort(listePays);
                        for (int i = 0; i < NBMAX; i++) {
                            pays = listePays.get(i);
                    %>
                    <tr>
                        <td><img src="<%= "/SuperApp" + pays.getDrapeauUrl()%>"></td>
                        <td><%= pays.getNom()%></td>
                    </tr>
                    <%  } %>




                    <tr><td colspan="2"><h1>Méthode 2 : Scriptlet, utilisation du Set</h1></td></tr>
                    <%
                        competiteurs = (Competiteurs) application.getAttribute("competiteurs");
                        // ON fabrique une liste pour pouvoir trier les données
                        // listePays = ((Set<Pays>) competiteurs.getPaysS()).stream().collect(Collectors.toList());
                        //Collections.sort(listePays, (o1, o2) -> o1.getNom().compareTo(o2.getNom()));
                        listePays = new ArrayList<>((Set<Pays>) competiteurs.getPaysS());
                        Collections.sort(listePays);
                        // Utilisation de foreach pour hanger
                        for (Pays pays : listePays) {
                    %>
                    <tr>
                        <td><img src="<%= "/SuperApp" + pays.getDrapeauUrl()%>"></td>
                        <td><%= pays.getNom()%></td>
                    </tr>
                    <%  } %>                    





                    <tr><td colspan="2"><h1>Méthode 3 : Scriptlet, utilisation du Map</h1></td></tr>
                    <%
                        competiteurs = (Competiteurs) application.getAttribute("competiteurs");
                        // Ici le tri est assuré sur la clé car c'est un TreeMap
                        for (Map.Entry<String, Pays> entry : ((Map<String, Pays>) competiteurs.getPaysM()).entrySet()) {
                            Pays pays = entry.getValue();
                    %>
                    <tr>
                        <td><img src="<%= "/SuperApp" + pays.getDrapeauUrl()%>"></td>
                        <td><%= pays.getNom()%></td>
                    </tr>
                    <%  }%>    



                </table>               
            </div>--> 
                    <h1>La bonne méthode JSTL et EL</h1>
                    <div>
                        <table>   
                            <c:forEach items="${competiteurs.paysL}" var="pays">
                                <tr>
                                    <td><img src="/SuperApp/${pays.drapeauUrl}"></td>
                                    <td>${pays.nom}</td>
                                </tr>
                            </c:forEach>
                        </table>
                    </div>
            </div>
    </body>
</html>
