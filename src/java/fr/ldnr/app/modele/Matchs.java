/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.ldnr.app.modele;

import java.util.Date;

/**
 *
 * @author stag
 */
public class Matchs {
    private String score1 = "-";
    private String score2 = "-";
    private Date date_prevue;
    Pays pays1 = new Pays();
    Pays pays2 = new Pays();
    

    public Matchs() 
    {
        
    }
    
    public Matchs(Pays pays1, Pays pays2, String score1, String score2, Date date_prevue) {
        this.pays1=pays1;
        this.pays2=pays2;
        this.score1=score1;
        this.score2=score2;
        this.date_prevue=date_prevue;
    }
    
    

    public String getScore1() {
        return score1;
    }

    public void setScore(String score1) {
        this.score1 = score1;
    }

    public Date getDate_prevue() {
        return date_prevue;
    }

    public void setDate_prevue(Date date_prevue) {
        this.date_prevue = date_prevue;
    }

    public String getScore2() {
        return score2;
    }

    public void setScore2(String score2) {
        this.score2 = score2;
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
