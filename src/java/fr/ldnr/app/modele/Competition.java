/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.ldnr.app.modele;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author stag
 */
public class Competition {
    private List<Matchs> matchL  = new ArrayList<>();
    
    
    public Competition() {
    }
    
    public void add(Matchs matchs) {
        matchL.add(matchs);
    }

    public List<Matchs> getMatchL() {
        return matchL;
    }

    public void setMatchL(List<Matchs> matchL) {
        this.matchL = matchL;
    }
}

